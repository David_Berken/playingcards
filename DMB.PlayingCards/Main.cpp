// Playing Cards
// David Berken

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit {Hearts, Diamonds, Spades, Clubs};
struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{
	(void)_getch();
	return 0;
}